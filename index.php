<?php

require_once 'vendor/autoload.php';

$app = new Silex\Application();
$client = new Github\Client();

$app->register(new Silex\Provider\TwigServiceProvider(), [
    'twig.path' => __DIR__ . '/views',
]);

$app->get('/', function() use($client, $app) {

    $repositories = $client->api('user')->repositories('symfony');

    return $app['twig']->render('lista.twig', [
                'repositories' => $repositories
    ]);
});

$app->run();
